
package com.tech.blog.dao;
import com.tech.blog.entities.User;
import java.sql.*;
public class UserDao {
    private Connection con;

    public UserDao(Connection con) {
        this.con = con;
    }
    
    public boolean saveUser(User user){
         boolean f=false;
       try{
          String query="insert into user(name,email,password,gender,about)values(?,?,?,?,?)"; 
          PreparedStatement pt=this.con.prepareStatement(query);
          pt.setString(1, user.getName());
          pt.setString(2, user.getEmail());
          pt.setString(3, user.getPassword());
          pt.setString(4, user.getGender());
          pt.setString(5, user.getAbout());
          pt.executeUpdate();
          f=true;
       } catch(Exception e){
           e.printStackTrace();
       }
       return f;
    }
    
    //get user by email and password
    public User getUserByEmailAndPassword( String email, String password){
        User user=null;
        
        try{
            String query="select *from user where email=? and password=?";
            PreparedStatement pstmt=con.prepareStatement(query);
            pstmt.setString(1, email);
            pstmt.setString(2, password);
            ResultSet rs=pstmt.executeQuery();
            if(rs.next()){
                user=new User();
                String name=rs.getString("name");
                user.setName(name);
                user.setId(rs.getInt("id"));
                user.setEmail(rs.getString("email"));
                user.setPassword(rs.getString("password"));
                user.setGender(rs.getString("gender"));
                user.setAbout(rs.getString("about"));
                user.setProfile(rs.getString("profile"));
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        
        
        
        return user;
    }
   
    public boolean updateUser(User user){
        boolean f=false;
        try{
           String query="update user set name=? , email=? , about=? , profile=? where id=?";
           PreparedStatement p=con.prepareStatement(query);
           p.setString(1, user.getName());
           p.setString(2, user.getEmail());
           p.setString(3, user.getAbout());
           p.setString(4, user.getProfile());
           p.setInt(5, user.getId());
           p.executeUpdate();
           f=true;
        }catch(Exception e){
            e.printStackTrace();
        }
       return f;
    }
}
