<%@page import="com.tech.blog.entities.Message"%>
<%@page import="com.tech.blog.entities.User" %>
<%@page errorPage="error_page.jsp" %>
<%
User user=(User)session.getAttribute("currentUser");
if(user==null){
response.sendRedirect("login_page.jsp");
    }
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Profile Page</title>
        
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css" integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous">
        <link href="css/mystyle.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <style>
            .banner-background{
               clip-path: polygon(30% 0%, 70% 0%, 100% 0, 100% 94%, 66% 100%, 25% 93%, 0 100%, 0 0);
            }
        </style>
        
        
    </head>
    <body>
       
               <nav class="navbar navbar-expand-lg navbar-dark primary-background">
           <a class="navbar-brand" href="index.jsp"><span class="fa fa-group"></span>TechBlog</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
          <a class="nav-link" href="#"><span class="fa fa-caret-square-o-right"></span>LearnCode Online <span class="sr-only">(current)</span></a>
      </li>
      
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <span class="fa fa-folder-open-o"></span> Categories
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="#">Programming Language</a>
          <a class="dropdown-item" href="#">Project Implementation</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="#">Data Structure</a>
        </div>
      </li>
      <li class="nav-item">
          <a class="nav-link" href="#"><span class="fa fa-address-book-o"></span>Contact</a>
      </li>
    </ul>
      <ul class="navbar-nav mr-right">
           <li class="nav-item">
               <a class="nav-link" href="#!" data-toggle="modal" data-target="#profile-modal"><span class="fa fa-user-circle"></span><%=user.getName() %></a>
      </li>
        <li class="nav-item">
          <a class="nav-link" href="LogoutServlet"><span class="fa fa-user-plus"></span>Logout</a>
      </li>  
      </ul>
  </div>
</nav>
      
       <%
                            Message m=(Message)session.getAttribute("msg");
                            if(m!=null){
                              %>
                              <div class="alert <%=m.getCssClass() %>" role="alert">
                                  <%= m.getContent() %>
                                </div>
                              <%
                                  session.removeAttribute("msg");
                            }
                            %>

<!-- Modal -->
<div class="modal fade" id="profile-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header primary-background text-white text-center">
        <h5 class="modal-title" id="exampleModalLabel">TechBlogs</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <div class="container text-center">
              <img src="pics/<%=user.getProfile()%>" class="img-fluid" style="border-radius: 50%">
                   <br>
              <h5 class="modal-title mt-3" id="exampleModalLabel"><%=user.getName() %></h5>
              <div id="profile-details">
              <table class="table">
  
  <tbody>
    <tr>
      <th scope="row">ID :</th>
      <td><%=user.getId()%></td>
      
    </tr>
    <tr>
      <th scope="row">Email :</th>
      <td><%=user.getEmail()%></td>
      
    </tr>
    <tr>
      <th scope="row">Status</th>
      <td><%=user.getAbout()%></td>
      
    </tr>
  </tbody>
</table>
              </div>
      <div id="profile-edit" style="display: none">
          
          <h3 class="mt-2">Please Edit Carefully</h3> 
          <form action="EditServlet" method="post" enctype="multipart/form-data">
              <table class="table">
                  <tr>
                      <td>ID :</td>
                      <td><%=user.getId()%></td>
                  </tr>
                   <tr>
                      <td>Name :</td>
                      <td><input type="name" name="user_name" class="form-control" value="<%=user.getName()%>"> </td>
                  </tr>
                  <tr>
                      <td>Email :</td>
                      <td><input type="email" name="user_email" class="form-control" value="<%=user.getEmail()%>"> </td>
                  </tr>
                    <tr>
                      <td>Status :</td>
                      <td><textarea class="form-control" name="user_about">
                          <%=user.getAbout()%>
                          </textarea></td>
                  </tr>
                    <tr>
                      <td>Profile Picture :</td>
                      <td><input type="file" name="image" class="form-control"> </td>
                  </tr>
                 

              </table>
                          <div class="container">
                              <button type="submit" class="btn btn-outline-primary">
                                  Save
                              </button>
                          </div>
          </form>
      </div>
              
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button id="edit-profile-button" type="button" class="btn btn-primary">EDIT</button>
      </div>
    </div>
  </div>
</div>

   
        
        
        <script
			  src="https://code.jquery.com/jquery-3.6.0.min.js"
			  integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4="
			  crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script src="js/myjs.js" type="text/javascript"></script>
<script>
   $(document).ready(function(){
       
       let editStatus=false;
       
      $('#edit-profile-button').click(function(){
         if(editStatus==false){
              $("#profile-details").hide()
          $("#profile-edit").show();
          editStatus=true;
          $(this).text("Back")
         }else{
            $("#profile-details").show()
          $("#profile-edit").hide();
          editStatus=false; 
          $(this).text("Edit")
         }
      }) 
   }) ;
</script>
        
    </body>
</html>
